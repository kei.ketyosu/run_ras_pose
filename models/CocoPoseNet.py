import numpy as np
import chainer
import chainer.functions as F
import chainer.links as L
from chainer.links import caffe
from chainer import initializers


initialW = initializers.HeNormal(1 / np.sqrt(2), np.float32)

def copy_vgg_params(model):
    print('Copying params of pretrained model...')
    layer_names = [
        "conv1_1", "conv1_2", "conv2_1", "conv2_2", "conv3_1",
        "conv3_2", "conv3_3", "conv3_4", "conv4_1", "conv4_2",
    ]
    pre_model = caffe.CaffeFunction('models/VGG_ILSVRC_19_layers.caffemodel')
    for layer_name in layer_names:
        exec("model.%s.W.data = pre_model['%s'].W.data" % (layer_name, layer_name))
        exec("model.%s.b.data = pre_model['%s'].b.data" % (layer_name, layer_name))
    print('Done.')

class DepthwiseDelatedConvolution(chainer.Chain):
    def __init__(self, ksize=3, stride=1, pad=1, dilate=1):
        super(DepthwiseDelatedConvolution, self).__init__()
        with self.init_scope():
            self.ch0 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch1 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch2 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch3 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch4 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch5 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch6 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)
            self.ch7 = L.Convolution2D(1, 1, ksize=ksize, stride=stride, pad=pad,
                                    initialW=initialW, nobias=True, dilate=dilate)

    def __call__(self, x):
        h = F.split_axis(x, 8, 1)
        h0 = self.ch0(h[0])
        h1 = self.ch1(h[1])
        h2 = self.ch2(h[2])
        h3 = self.ch3(h[3])
        h4 = self.ch4(h[4])
        h5 = self.ch5(h[5])
        h6 = self.ch6(h[6])
        h7 = self.ch7(h[7])
        return F.concat((h0,h1,h2,h3,h4,h5,h6,h7), axis=1)

class Stage_0(chainer.Chain):
    def __init__(self):
        super(Stage_0, self).__init__()
        with self.init_scope():
            self.conv0 = L.Convolution2D(3,2,ksize=3, stride=2, pad=1, initialW=initialW, nobias=True)
            self.conv1 = L.Convolution2D(5,2,ksize=3, stride=1, pad=1, dilate=1, initialW=initialW, nobias=True)
            self.bn0 = L.BatchNormalization(2, eps=0.001, decay=0.9997)
    def __call__(self, x):
        h0 = F.clipped_relu(self.bn0(self.conv0(x)), 1.0)
        h = F.average_pooling_2d(x, ksize=2, stride=2)
        h1 = F.clipped_relu(self.conv1(F.concat((h , h0), axis=1)), 1.0)
        return F.concat((h0, h1), axis=1)

class N_Stage(chainer.Chain):
    def __init__(self, in_channels=61):
        super(N_Stage, self).__init__()

        base_channels = 8

        with self.init_scope():
            self.conv_IN = L.Convolution2D(in_channels,
                                        base_channels,
                                        ksize=1,
                                        stride=1,
                                        pad=0,
                                        initialW=initialW,
                                        nobias=True)
            self.bn_IN = L.BatchNormalization(base_channels,
                                           eps=0.001, decay=0.9997)

            self.conv_3x3_0 = L.DepthwiseConvolution2D(base_channels,
                                                           channel_multiplier=2,
                                                           ksize=3,
                                                           stride=1,
                                                           pad=1,
                                                           initialW=initialW,
                                                           nobias=True)
            self.conv_3x3_1 = L.DepthwiseConvolution2D(base_channels,
                                                           channel_multiplier=2,
                                                           ksize=3,
                                                           stride=1,
                                                           pad=1,
                                                           initialW=initialW,
                                                           nobias=True)
            # self.conv_3x3_2 = L.DepthwiseConvolution2D(base_channels,
            #                                                channel_multiplier=1,
            #                                                ksize=3,
            #                                                stride=1,
            #                                                pad=1,
            #                                                initialW=initialW,
            #                                                nobias=True)
            # self.conv_3x3_3 = L.DepthwiseConvolution2D(base_channels,
            #                                                channel_multiplier=2,
            #                                                ksize=3,
            #                                                stride=1,
            #                                                pad=1,
            #                                                initialW=initialW,
            #                                                nobias=True)
            #self.conv_7x7_0 = DepthwiseDelatedConvolution(ksize=3, stride=1, pad=3, dilate=3)
            #self.conv_7x7_1 = DepthwiseDelatedConvolution(ksize=3, stride=1, pad=3, dilate=3)
            # self.conv_3x3_0 = L.DepthwiseConvolution2D(base_channels,
            #                                                channel_multiplier=1,
            #                                                ksize=3,
            #                                                stride=1,
            #                                                pad=1,
            #                                                initialW=initialW,
            #                                                nobias=True)
            # self.conv_3x3_1 = L.DepthwiseConvolution2D(base_channels,
            #                                                channel_multiplier=1,
            #                                                ksize=3,
            #                                                stride=1,
            #                                                pad=1,
            #                                                initialW=initialW,
            #                                                nobias=True)
            # self.conv_3x3_2 = L.DepthwiseConvolution2D(base_channels,
            #                                                channel_multiplier=1,
            #                                                ksize=3,
            #                                                stride=1,
            #                                                pad=1,
            #                                                initialW=initialW,
            #                                                nobias=True)
            #self.conv_5x5_0 = DepthwiseDelatedConvolution(ksize=3, stride=1, pad=2, dilate=2)
            #self.conv_5x5_1 = DepthwiseDelatedConvolution(ksize=3, stride=1, pad=2, dilate=2)
            #self.conv_7x7 = DepthwiseDelatedConvolution(ksize=3, stride=1, pad=3, dilate=3)

            # self.conv_OUT0_0 = L.Convolution2D(base_channels * 4,
            #                             base_channels * 4,
            #                             ksize=3,
            #                             stride=1,
            #                             pad=1,
            #                             initialW=initialW,
            #                             nobias=True)
            # self.conv_OUT1_0 = L.Convolution2D(base_channels * 4,
            #                             base_channels * 4,
            #                             ksize=3,
            #                             stride=1,
            #                             pad=1,
            #                             initialW=initialW,
            #                             nobias=True)
            # self.bn_OUT_0 = L.BatchNormalization(base_channels * 4,
            #                                eps=0.001, decay=0.9997)
            # self.bn_OUT_1 = L.BatchNormalization(base_channels * 4,
            #                                eps=0.001, decay=0.9997)
            self.conv_OUT0_1 = L.Convolution2D(base_channels * 2,
                                        38,
                                        ksize=1,
                                        stride=1,
                                        pad=0,
                                        initialW=initialW,
                                        nobias=False)
            self.conv_OUT1_1 = L.Convolution2D(base_channels * 2,
                                        19,
                                        ksize=1,
                                        stride=1,
                                        pad=0,
                                        initialW=initialW,
                                        nobias=False)

    def __call__(self, x):
        h = F.clipped_relu(self.bn_IN(self.conv_IN(x) ), 1.0)
        #h = self.conv_3x3_0(h)
        #h1 = self.conv_3x3_1(h)
        #h2 = self.conv_3x3_2(h)
        #h2 = self.conv_5x5_0(h)
        #h3 = self.conv_5x5_1(h)
        #h3 = self.conv_7x7(h)
        #h = F.clipped_relu(F.concat((h0, h1, h2, h3), axis=1), 1.0)
        h0 = F.clipped_relu(self.conv_3x3_0(h), 1.0)
        h1 = F.clipped_relu(self.conv_3x3_1(h), 1.0)
        # h1 = F.clipped_relu(self.conv_3x3_2(h1), 1.0)
        # h1 = F.clipped_relu(self.conv_3x3_3(h1), 1.0)

        h0 = self.conv_OUT0_1(h0)
        h1 = self.conv_OUT1_1(h1)
        # h0 = F.clipped_relu(self.bn_OUT_0(self.conv_OUT0_0(h)), 1.0)
        # h1 = F.clipped_relu(self.bn_OUT_1(self.conv_OUT1_0(h)), 1.0)
        # h0 = self.conv_OUT0_1(h0)
        # h1 = self.conv_OUT1_1(h1)
        return h0, h1


class CocoPoseNet(chainer.Chain):
    insize = 64

    def __init__(self):
        super(CocoPoseNet, self).__init__(
            # # cnn to make feature map
            # conv1_1=L.Convolution2D(in_channels=3, out_channels=64, ksize=3, stride=1, pad=1),
            # conv1_2=L.Convolution2D(in_channels=64, out_channels=64, ksize=3, stride=1, pad=1),
            # conv2_1=L.Convolution2D(in_channels=64, out_channels=128, ksize=3, stride=1, pad=1),
            # conv2_2=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv3_1=L.Convolution2D(in_channels=128, out_channels=256, ksize=3, stride=1, pad=1),
            # conv3_2=L.Convolution2D(in_channels=256, out_channels=256, ksize=3, stride=1, pad=1),
            # conv3_3=L.Convolution2D(in_channels=256, out_channels=256, ksize=3, stride=1, pad=1),
            # conv3_4=L.Convolution2D(in_channels=256, out_channels=256, ksize=3, stride=1, pad=1),
            # conv4_1=L.Convolution2D(in_channels=256, out_channels=512, ksize=3, stride=1, pad=1),
            # conv4_2=L.Convolution2D(in_channels=512, out_channels=512, ksize=3, stride=1, pad=1),
            # conv4_3_CPM=L.Convolution2D(in_channels=512, out_channels=256, ksize=3, stride=1, pad=1),
            # conv4_4_CPM=L.Convolution2D(in_channels=256, out_channels=128, ksize=3, stride=1, pad=1),

            # # stage1
            # conv5_1_CPM_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv5_2_CPM_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv5_3_CPM_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv5_4_CPM_L1=L.Convolution2D(in_channels=128, out_channels=512, ksize=1, stride=1, pad=0),
            # conv5_5_CPM_L1=L.Convolution2D(in_channels=512, out_channels=38, ksize=1, stride=1, pad=0),
            # conv5_1_CPM_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv5_2_CPM_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv5_3_CPM_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=3, stride=1, pad=1),
            # conv5_4_CPM_L2=L.Convolution2D(in_channels=128, out_channels=512, ksize=1, stride=1, pad=0),
            # conv5_5_CPM_L2=L.Convolution2D(in_channels=512, out_channels=19, ksize=1, stride=1, pad=0),

            # # stage2
            # Mconv1_stage2_L1=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage2_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage2_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage2_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage2_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage2_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage2_L1=L.Convolution2D(in_channels=128, out_channels=38, ksize=1, stride=1, pad=0),
            # Mconv1_stage2_L2=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage2_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage2_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage2_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage2_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage2_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage2_L2=L.Convolution2D(in_channels=128, out_channels=19, ksize=1, stride=1, pad=0),

            # # stage3
            # Mconv1_stage3_L1=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage3_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage3_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage3_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage3_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage3_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage3_L1=L.Convolution2D(in_channels=128, out_channels=38, ksize=1, stride=1, pad=0),
            # Mconv1_stage3_L2=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage3_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage3_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage3_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage3_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage3_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage3_L2=L.Convolution2D(in_channels=128, out_channels=19, ksize=1, stride=1, pad=0),

            # # stage4
            # Mconv1_stage4_L1=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage4_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage4_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage4_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage4_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage4_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage4_L1=L.Convolution2D(in_channels=128, out_channels=38, ksize=1, stride=1, pad=0),
            # Mconv1_stage4_L2=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage4_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage4_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage4_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage4_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage4_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage4_L2=L.Convolution2D(in_channels=128, out_channels=19, ksize=1, stride=1, pad=0),

            # # stage5
            # Mconv1_stage5_L1=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage5_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage5_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage5_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage5_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage5_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage5_L1=L.Convolution2D(in_channels=128, out_channels=38, ksize=1, stride=1, pad=0),
            # Mconv1_stage5_L2=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage5_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage5_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage5_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage5_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage5_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage5_L2=L.Convolution2D(in_channels=128, out_channels=19, ksize=1, stride=1, pad=0),

            # # stage6
            # Mconv1_stage6_L1=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage6_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage6_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage6_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage6_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage6_L1=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage6_L1=L.Convolution2D(in_channels=128, out_channels=38, ksize=1, stride=1, pad=0),
            # Mconv1_stage6_L2=L.Convolution2D(in_channels=185, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv2_stage6_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv3_stage6_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv4_stage6_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv5_stage6_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=7, stride=1, pad=3),
            # Mconv6_stage6_L2=L.Convolution2D(in_channels=128, out_channels=128, ksize=1, stride=1, pad=0),
            # Mconv7_stage6_L2=L.Convolution2D(in_channels=128, out_channels=19, ksize=1, stride=1, pad=0),
        )
        with self.init_scope():
            self.stage0 = Stage_0()
            self.stage1 = N_Stage(4)
            self.stage2 = N_Stage()
            self.stage3 = N_Stage()
            self.stage4 = N_Stage()
            self.stage5 = N_Stage()

    def __call__(self, x):
        heatmaps = []
        pafs = []

        h = self.stage0(x)

        h0, h1 = self.stage1(h)
        pafs.append(h0)
        heatmaps.append(h1)

        h0, h1 = self.stage2(F.concat((h, h0, h1), axis=1))
        pafs.append(h0)
        heatmaps.append(h1)

        h0, h1 = self.stage3(F.concat((h, h0, h1), axis=1))
        pafs.append(h0)
        heatmaps.append(h1)

        h0, h1 = self.stage4(F.concat((h, h0, h1), axis=1))
        pafs.append(h0)
        heatmaps.append(h1)

        h0, h1 = self.stage5(F.concat((h, h0, h1), axis=1))
        pafs.append(h0)
        heatmaps.append(h1)

        # h = F.relu(self.conv1_1(x))
        # h = F.relu(self.conv1_2(h))
        # h = F.max_pooling_2d(h, ksize=2, stride=2)
        # h = F.relu(self.conv2_1(h))
        # h = F.relu(self.conv2_2(h))
        # h = F.max_pooling_2d(h, ksize=2, stride=2)
        # h = F.relu(self.conv3_1(h))
        # h = F.relu(self.conv3_2(h))
        # h = F.relu(self.conv3_3(h))
        # h = F.relu(self.conv3_4(h))
        # h = F.max_pooling_2d(h, ksize=2, stride=2)
        # h = F.relu(self.conv4_1(h))
        # h = F.relu(self.conv4_2(h))
        # h = F.relu(self.conv4_3_CPM(h))
        # h = F.relu(self.conv4_4_CPM(h))
        # feature_map = h

        # # stage1
        # h1 = F.relu(self.conv5_1_CPM_L1(feature_map)) # branch1
        # h1 = F.relu(self.conv5_2_CPM_L1(h1))
        # h1 = F.relu(self.conv5_3_CPM_L1(h1))
        # h1 = F.relu(self.conv5_4_CPM_L1(h1))
        # h1 = self.conv5_5_CPM_L1(h1)
        # h2 = F.relu(self.conv5_1_CPM_L2(feature_map)) # branch2
        # h2 = F.relu(self.conv5_2_CPM_L2(h2))
        # h2 = F.relu(self.conv5_3_CPM_L2(h2))
        # h2 = F.relu(self.conv5_4_CPM_L2(h2))
        # h2 = self.conv5_5_CPM_L2(h2)
        # pafs.append(h1)
        # heatmaps.append(h2)

        # # stage2
        # h = F.concat((h1, h2, feature_map), axis=1) # channel concat
        # h1 = F.relu(self.Mconv1_stage2_L1(h)) # branch1
        # h1 = F.relu(self.Mconv2_stage2_L1(h1))
        # h1 = F.relu(self.Mconv3_stage2_L1(h1))
        # h1 = F.relu(self.Mconv4_stage2_L1(h1))
        # h1 = F.relu(self.Mconv5_stage2_L1(h1))
        # h1 = F.relu(self.Mconv6_stage2_L1(h1))
        # h1 = self.Mconv7_stage2_L1(h1)
        # h2 = F.relu(self.Mconv1_stage2_L2(h)) # branch2
        # h2 = F.relu(self.Mconv2_stage2_L2(h2))
        # h2 = F.relu(self.Mconv3_stage2_L2(h2))
        # h2 = F.relu(self.Mconv4_stage2_L2(h2))
        # h2 = F.relu(self.Mconv5_stage2_L2(h2))
        # h2 = F.relu(self.Mconv6_stage2_L2(h2))
        # h2 = self.Mconv7_stage2_L2(h2)
        # pafs.append(h1)
        # heatmaps.append(h2)

        # # stage3
        # h = F.concat((h1, h2, feature_map), axis=1) # channel concat
        # h1 = F.relu(self.Mconv1_stage3_L1(h)) # branch1
        # h1 = F.relu(self.Mconv2_stage3_L1(h1))
        # h1 = F.relu(self.Mconv3_stage3_L1(h1))
        # h1 = F.relu(self.Mconv4_stage3_L1(h1))
        # h1 = F.relu(self.Mconv5_stage3_L1(h1))
        # h1 = F.relu(self.Mconv6_stage3_L1(h1))
        # h1 = self.Mconv7_stage3_L1(h1)
        # h2 = F.relu(self.Mconv1_stage3_L2(h)) # branch2
        # h2 = F.relu(self.Mconv2_stage3_L2(h2))
        # h2 = F.relu(self.Mconv3_stage3_L2(h2))
        # h2 = F.relu(self.Mconv4_stage3_L2(h2))
        # h2 = F.relu(self.Mconv5_stage3_L2(h2))
        # h2 = F.relu(self.Mconv6_stage3_L2(h2))
        # h2 = self.Mconv7_stage3_L2(h2)
        # pafs.append(h1)
        # heatmaps.append(h2)

        # # stage4
        # h = F.concat((h1, h2, feature_map), axis=1) # channel concat
        # h1 = F.relu(self.Mconv1_stage4_L1(h)) # branch1
        # h1 = F.relu(self.Mconv2_stage4_L1(h1))
        # h1 = F.relu(self.Mconv3_stage4_L1(h1))
        # h1 = F.relu(self.Mconv4_stage4_L1(h1))
        # h1 = F.relu(self.Mconv5_stage4_L1(h1))
        # h1 = F.relu(self.Mconv6_stage4_L1(h1))
        # h1 = self.Mconv7_stage4_L1(h1)
        # h2 = F.relu(self.Mconv1_stage4_L2(h)) # branch2
        # h2 = F.relu(self.Mconv2_stage4_L2(h2))
        # h2 = F.relu(self.Mconv3_stage4_L2(h2))
        # h2 = F.relu(self.Mconv4_stage4_L2(h2))
        # h2 = F.relu(self.Mconv5_stage4_L2(h2))
        # h2 = F.relu(self.Mconv6_stage4_L2(h2))
        # h2 = self.Mconv7_stage4_L2(h2)
        # pafs.append(h1)
        # heatmaps.append(h2)

        # # stage5
        # h = F.concat((h1, h2, feature_map), axis=1) # channel concat
        # h1 = F.relu(self.Mconv1_stage5_L1(h)) # branch1
        # h1 = F.relu(self.Mconv2_stage5_L1(h1))
        # h1 = F.relu(self.Mconv3_stage5_L1(h1))
        # h1 = F.relu(self.Mconv4_stage5_L1(h1))
        # h1 = F.relu(self.Mconv5_stage5_L1(h1))
        # h1 = F.relu(self.Mconv6_stage5_L1(h1))
        # h1 = self.Mconv7_stage5_L1(h1)
        # h2 = F.relu(self.Mconv1_stage5_L2(h)) # branch2
        # h2 = F.relu(self.Mconv2_stage5_L2(h2))
        # h2 = F.relu(self.Mconv3_stage5_L2(h2))
        # h2 = F.relu(self.Mconv4_stage5_L2(h2))
        # h2 = F.relu(self.Mconv5_stage5_L2(h2))
        # h2 = F.relu(self.Mconv6_stage5_L2(h2))
        # h2 = self.Mconv7_stage5_L2(h2)
        # pafs.append(h1)
        # heatmaps.append(h2)

        # # stage6
        # h = F.concat((h1, h2, feature_map), axis=1) # channel concat
        # h1 = F.relu(self.Mconv1_stage6_L1(h)) # branch1
        # h1 = F.relu(self.Mconv2_stage6_L1(h1))
        # h1 = F.relu(self.Mconv3_stage6_L1(h1))
        # h1 = F.relu(self.Mconv4_stage6_L1(h1))
        # h1 = F.relu(self.Mconv5_stage6_L1(h1))
        # h1 = F.relu(self.Mconv6_stage6_L1(h1))
        # h1 = self.Mconv7_stage6_L1(h1)
        # h2 = F.relu(self.Mconv1_stage6_L2(h)) # branch2
        # h2 = F.relu(self.Mconv2_stage6_L2(h2))
        # h2 = F.relu(self.Mconv3_stage6_L2(h2))
        # h2 = F.relu(self.Mconv4_stage6_L2(h2))
        # h2 = F.relu(self.Mconv5_stage6_L2(h2))
        # h2 = F.relu(self.Mconv6_stage6_L2(h2))
        # h2 = self.Mconv7_stage6_L2(h2)
        # pafs.append(h1)
        # heatmaps.append(h2)

        return pafs, heatmaps
